program hsc_tool;
{$librarypath '../blibs/'}
// get your blibs here: https://gitlab.com/bocianu/blibs
uses atari, crt, b_crt, fn_sio, sio, fn_cookies;
{$r resources.rc}
{$i-}

const
{$i const.inc}
{$i types.inc}

const
    APPKEY_CREATOR_ID = $b0c1;
    APPKEY_APP_ID = $ca;
    APPKEY_AUTH_KEY = $fe;

var 
	uri:string[80];    
	s: string[64];
	gid: string[3];
	ioStatus: FN_StatusStruct;
    responseBuffer: array [0..4095] of byte absolute BUFFER;

    logo: array [0..13*4-1] of byte = (
        $00, $00, $00, $00, $00, $5a, $5b, $5c, $00, $00, $00, $00, $00, 
        $40, $41, $42, $43, $44, $54, $55, $56, $4a, $4b, $4c, $4d, $4e, 
        $45, $46, $47, $48, $49, $57, $58, $59, $4f, $50, $51, $52, $53, 
        $00, $00, $00, $00, $00, $00, $5d, $5e, $5f, $00, $00, $00, $00
    );
	credentials: TCredentials;
	credLen: byte;
	savingEnabled : boolean = true;
	k:char;
	b:byte;
	colon:string[1]=':';
	dataSize:word;
	username,password:string[16];
	FujiNetPresent,refresh,credIsset,credValid:boolean;

	last_entry:boolean;
	listptr:word;
	names:array[0..32*LIST_LEN-1] of char;
	nameptr:word;
	ids:array[0..4*LIST_LEN-1] of char;
	idsptr:word;
    
// ***************************************************** HELPERS    

procedure ScreenOff;
begin
    sdmctl := sdmctl and %11111100;
end;

procedure ScreenOn;
begin
    sdmctl := sdmctl or %00100010;
end;

procedure MergeStr(var s1:string;s2:string[40]);
var l1,l2:byte;
begin
    l1 := Length(s1);
    l2 := Length(s2);
    s1[0] := char(l1 + l2);
    while l2>0 do begin
        s1[l1+l2] := s2[l2];
        dec(l2);
    end;
end;

procedure DecodeUserPass;
var i,sep,pstart:byte;
const USTART = 11;
begin
	s[0]:=char(credLen);
	move(credentials,s[1],credLen);
	sep := 0;
	for i:=USTART to 24 do begin
		if s[i] = ':' then begin
			sep := i;
			Pause;
		end;
	end;
	if sep = 0 then exit;
	move(s[USTART],username[1],sep-USTART);
	username[0] := char(sep-USTART);
	pstart := sep+1;
	sep := 0;
	for i:=pstart to 40 do begin
		if s[i] = '@' then begin
			sep := i;
			Pause;
		end;
	end;
	if sep = 0 then exit;
	move(s[pstart],password[1],sep-pstart);
	password[0] := char(sep-pstart);
	credIsset := true;
end;

procedure EncodeUserPass;
begin
	s:='N:https://';
	MergeStr(s,username);
	MergeStr(s,colon);
	MergeStr(s,password);
	MergeStr(s,'@atari.pl/hsc/?x='#0);
	credLen := byte(s[0]);
	move(s[1],@credentials,credLen);
end;

procedure SetGid(id:byte);	
var i,j,idptr:byte;
begin
	gid:='000';
	j:=3;
	idptr:=id shl 2;
	for i:=2 downto 0 do begin
		if ids[idptr+i] <> #0 then begin
			gid[j]:=ids[idptr+i];
			dec(j);
		end;
	end;
end;
	
function isGame(b:byte):boolean;
begin
	result := b < LIST_LEN;
end;		

function PromptEntry:byte;
begin	
	result := ord(readkey)-97;
end;	

function ValidateEntry(var s:string):boolean;
var l,i:byte;
begin
	result := true;
	l := Length(s);
	if l=0 then exit(false);
	for i:=1 to l do
		if (s[i]<'0') or (s[i]>'9') then exit(false);

end;
	

// ***************************************************** COOKIE ROUTINES


function LoadCredentials: byte;
begin
    InitCookie(APPKEY_CREATOR_ID, APPKEY_APP_ID, APPKEY_AUTH_KEY);
    result := GetCookie(credentials);
end;

function SaveCredentials: byte;
begin
    InitCookie(APPKEY_CREATOR_ID, APPKEY_APP_ID, APPKEY_AUTH_KEY);
    result := $ff;
    if savingEnabled then result := SetCookie(credentials, credLen);
    if result <> 1 then savingEnabled := false;
end;

// ***************************************************** NETWORK ROUTINES

function WaitAndParseRequest:byte;
var 
	timeout:byte;
	bytesWaiting:word;
begin
    dataSize := 0;
    result := 1;
    timeout := 3;
    repeat 
        bytesWaiting := 0;
        FN_ReadStatus(ioStatus);
        if (ioStatus.errorCode <> 1) and (dataSize = 0) then exit(0);
        bytesWaiting := ioStatus.dataSize;
        if bytesWaiting > 0 then begin
            FN_ReadBuffer(@responseBuffer[dataSize], bytesWaiting);
            dataSize := dataSize + bytesWaiting;
        end;
        dec(timeout);
    until ((bytesWaiting = 0) and (dataSize <> 0)) or (timeout=0);
end;

function HTTPGet(var uri:string):boolean;
begin
	result := true;
    FN_Open(uri);
    if sioResult <> 1 then result := false;
    ioResult := WaitAndParseRequest;
    if ioResult <> 1 then result := false;
    FN_Close;    
end;

function FetchList:word;
begin
	s:= 'N:https://atari.pl/hsc/jgames.php'#0;
	HTTPGet(s);
	result := BUFFER;
end;	

function ValidateUserPass():boolean;
begin
	EncodeUserPass;
	uri[0] := char(credLen-1);
	move(credentials,uri[1],credLen-1);
	s:='1080'#0;
	MergeStr(uri,s);
    result := HTTPGet(uri);
end;
	
function FetchBoard(id:byte):word;
begin
	SetGid(id);
	s:= 'N:https://atari.pl/hsc/jboard.php?x=';
	MergeStr(s,gid);
	HTTPGet(s);
	result := BUFFER;
end;	

// ***************************************************** GUI ROUTINES

procedure ClearGfx;
begin
    FillByte(pointer(VRAMTXT),40*24,0);
end;

procedure ShowWelcomeMsg;
begin
    ClearGfx;
    // draw logo
    move(logo[0*13],pointer(savmsc+40*1+2),13);
    move(logo[1*13],pointer(savmsc+40*2+2),13);
    move(logo[2*13],pointer(savmsc+40*3+2),13);
    move(logo[3*13],pointer(savmsc+40*4+2),13);
    
    Gotoxy(17,3);
    Write('Hi Score Cafe Tool');
    Gotoxy(17,4);
    Write('by bocianu@gmail.com');
    Gotoxy(2,6);
    Writeln;

    CursorOff;
    ScreenOn;
end;

procedure ChangePass;
begin
	CursorOn;
	Writeln;
	Write('Enter username: ');
	Readln(username);
	Write('Enter password: ');
	Readln(password);
	Write('Validation: ');
	if ValidateUserPass() then begin
		Writeln('successfull!');
		credValid := true;
		b := SaveCredentials;
		if b = 1 then Writeln('Key successfuly saved')
		else begin
			Write('Error saving key: ');
			if b = 144 then Writeln('check your SD card') else Writeln(b);
		end;
	end else begin
		Writeln('failed!');
		Writeln;
		Writeln('Check your credentials and try again');
	end;
	Writeln;
	Writeln('Press ANY key.');
	refresh:=true;
	Readkey;
	CursorOff;
end;
		
procedure ShowGameHiScore(g:byte);
var i:byte;
	nameptr:word;
begin
	last_entry := true;
	nameptr := g shl 5;
	for i:=0 to 31 do begin
		if names[nameptr+i] = #0 then break;
		s[i+1] := char(byte(names[nameptr+i])+128);
	end;
	s[0]:=char(i);
    ClearGfx;
	Gotoxy(3,1);
	Writeln(' Hi Score Cafe  List of Top Players '*);
	Gotoxy((42 - Length(s)) shr 1,3);
	Writeln(s);
	FetchBoard(g);
	move(pointer(BUFFER),pointer(savmsc + 200),dataSize);

end;		
	
procedure LoadAndShowEntries(num:byte);	
var
	line:string[40];
	lnum,cnum:byte;
	top:word;
begin
	last_entry := true;
    ClearGfx;
	Gotoxy(3,1);
	Writeln('Select a game to view its scoreboard'*);
	Writeln;
	lnum:=0;
	top:=BUFFER + dataSize;
	repeat 
		line := '                                     ';
		line[1] := char(65+lnum+128);
		nameptr := lnum shl 5;
		idsptr := lnum shl 2;
		cnum:=3;
		repeat 
			Inc(listptr);
			if listptr>top then exit;
		until peek(listptr)=ord('"'); Inc(listptr);
		repeat 
			names[nameptr] := char(peek(listptr));
			if cnum<37 then line[cnum] := names[nameptr];
			Inc(listptr);
			Inc(nameptr);
			Inc(cnum);
			if listptr>top then exit;
		until peek(listptr)=ord('"'); Inc(listptr);
		repeat 
			Inc(listptr);
			if listptr>top then exit;
		until peek(listptr)=ord('"'); Inc(listptr);
		repeat 
			ids[idsptr] := char(peek(listptr));
			Inc(listptr);
			Inc(idsptr);
			if listptr>top then exit;
		until peek(listptr)=ord('"'); Inc(listptr);
		Writeln(line);
		inc(lnum)
	until lnum >= num;
	Gotoxy(3,24);
	Write(' Press space to go to the next page '*);

	if top-listptr>10 then last_entry := false;
end;	
	
function SelectGame():byte;
begin
	result := $ff;
	last_entry := false;
	listptr := FetchList;
	repeat 
		FillByte(names,32*LIST_LEN,0);
		FillByte(ids,4*LIST_LEN,0);
		LoadAndShowEntries(LIST_LEN);
		result := PromptEntry();
		if isGame(result) then exit(result)
		else result := $ff;	
	until last_entry;
end;	
	
function ShowHiScores():byte;
var g:byte;
begin
	g:=SelectGame();
	if g<>$ff then begin
		ShowGameHiScore(g);
		Gotoxy(3,24);
		Write(' Press any key to exit to main menu '*);
		Readkey;	
	end;
	refresh := true;
end;	

procedure EnterScore;
var g:byte;
	valid:boolean;
begin
	valid:=false;
	g:=SelectGame();
	if g<>$ff then begin 
		ShowGameHiScore(g);
		Gotoxy(3,17);
		CursorOn;
		Writeln('username: ',username);
		Writeln('You can type ''remove''');
		Writeln('to delete your last entry');
		Writeln;
		EncodeUserPass;
		Write('Enter your score: ');
		readln(s);
		CursorOff;
		move(credentials,uri[1],credLen-1);
		SetGid(g);

		if s='remove' then begin
			valid := true;
			uri[0] := char(credLen-4);
			MergeStr(uri,'fn.php?x=');
			MergeStr(uri,gid);
			Writeln('Removing last entry...');
		end else begin
			valid := ValidateEntry(s);
			if valid then begin
				uri[0] := char(credLen-1);
				MergeStr(uri,gid);
				MergeStr(uri,s);
				Writeln('Adding new score...');
			end;
		end;
		if valid then begin
			if HTTPGet(uri) then Writeln('OK - operation successful')
			else Writeln('KO - communication error');
			Readkey;
			ShowGameHiScore(g);
			Gotoxy(3,24);
			Write(' Press any key to exit to main menu '*);
		end
		else Writeln('Invalid entry. Operation aborted.');
		Readkey;	
	end;
	refresh := true;	
end;

// **********************************************************************
// *******************************************************************************  MAIN
// **********************************************************************

begin
	
    ScreenOff;
    Pause;
    SDLSTL := DLIST;
    nmien := $40; 
    savmsc := VRAMTXT;
	
    color1 := 10;
    color2 := $94;
    color4 := 0;

    // prepare logo charset
    move(pointer($e000), pointer(FONT), $400);
    move(pointer(LOGO_CHARSET), pointer(FONT + $200), $100);
    chbas := Hi(FONT); 
	Poke(702,0); // CAPS off
	
	repeat
		FujiNetPresent := false;
		credIsset := false;
		credValid := false;
		refresh := false;
		username := 'not set';
		credLen := 0;
		b := LoadCredentials;
		//144 - no cookie
		//144 - no sdcard
		//138 - no fujinet
		//139 - no fujinet
		if b = 144 then FujiNetPresent := true;
		if b = 1 then begin
			FujiNetPresent := true;
			credLen := cookie.len;
			DecodeUserPass;
		end;
		
		ShowWelcomeMsg;
		Write('FujiNet: ');
		if not FujiNetPresent then Writeln('not found') 
		else begin
			Writeln('detected');
			Write('Current user: ');
			Writeln(username);
			Writeln;
			Writeln('START'*+'  - change user/pass');
			Writeln('SELECT'*+' - view Hi Score Boards');
			if credIsset then Writeln('OPTION'*+' - edit your score manually');
		end;
		
		Writeln;
		if not FujiNetPresent then Writeln('R'*+' to retry');
		Writeln('Q'*+' to exit');

		repeat
			pause;

			if FujiNetPresent and CRT_StartPressed then ChangePass;
			if FujiNetPresent and CRT_SelectPressed then ShowHiScores;
			if FujiNetPresent and credIsset and CRT_OptionPressed then EnterScore;
			
			if keypressed then begin
				k := readkey;
				if (k = 'r') or (k = 'R') then refresh := true;
			end;
		until refresh or (k = 'q') or (k = 'Q');

	until (k = 'q') or (k = 'Q');

    TextMode(0);
end.
