;; Hi Score Cafe - saving routine prototype

APPKEYMODE_READ = 0;    

SIO_FUJICMD_READ_APPKEY = $DD; 
SIO_FUJICMD_OPEN_APPKEY = $DC;  
SIO_FUJICMD_CLOSE_APPKEY = $DB; 

_R = $40; // Read - predefined values for data transfer direction (to be set in DSTATS)
_W = $80; // Write
_NO = $00; // No data transfer

JSIOINT = $E459; // SIO OS procedure vector
DCB = $300

    org $2000
    
main

    ;; openCookie struct is set by deafault
    jsr execSio
    cpy #1
    bne error
    
    ;; getCookie 
    mwa #getCookieDCBstruct loadDCBloop+1
    jsr execSio
    cpy #1
    bne error
    mva #$b6 712

    ;; closeCookie 
    mwa #closeCookieDCBstruct loadDCBloop+1
    jsr execSio

    ;; concat uri + id + score
    ldx #0
    ldy urlLen
@    
    lda id_score,x
    sta hscAPI-1,y
    beq lastchar
    inx
    iny
    bne @-
lastchar

    ;; requestHTTP
    mwa #httpsRequestDCBstruct loadDCBloop+1
    jsr execSio
    cpy #1
    bne error
    
    mwa #httpsStatusDCBstruct loadDCBloop+1
    jsr execSio
    
    ;; closeHTTP
    mwa #httpsCloseDCBstruct loadDCBloop+1

execSio

    jsr loadDCB
    jsr JSIOINT 
    rts
error
    ;; some status value returned?
    rts

    ;; copies struct to DCB
loadDCB
    ldy #11 
loadDCBloop 
    lda openCookieDCBstruct,y 
    sta DCB,y
    dey
    bpl loadDCBloop 
    rts

;;;;;;;;;;;;;;; DATA

cookieInfo
    dta a($b0c1);; creator
    dta $ca     ;; app id
    dta $fe     ;; key
cookieMode
    dta APPKEYMODE_READ  ;; mode
    dta $0  ;; reserved/not used
    
;; DCB structs  
openCookieDCBstruct
    dta $70,1,SIO_FUJICMD_OPEN_APPKEY,_W,a(cookieInfo),1,0,a(6),0,0
getCookieDCBstruct
    dta $70,1,SIO_FUJICMD_READ_APPKEY,_R,a(buffer),1,0,a(66),0,0
closeCookieDCBstruct
    dta $70,1,SIO_FUJICMD_CLOSE_APPKEY,0,a(0),1,0,a(0),0,0
httpsRequestDCBstruct
    dta $71,1,'O',_W,a(hscAPI),3,0,a(256),4,0
httpsStatusDCBstruct
    dta $71,1,'S',_R,a(buffer),0,0,a(4),0,0 
httpsCloseDCBstruct
    dta $71,1,'C',0,a(0),0,0,a(0),0,0   

;; GAMEID + SCORE

id_score
    dta '108' ;; game ID
    dta '32'  ;; score
    dta 0     ;; EOL
    
;; here is place for data from cookie       
buffer        
urlLen
    .byte 0,0 ;; size of cookie data (url to API) will be set here
hscAPI
    ;; here goes connection string for fujinet 
    ;; ~80 free bytes needed, depends on url length
    run main
